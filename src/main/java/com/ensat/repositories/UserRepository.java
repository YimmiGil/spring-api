package com.ensat.repositories;

import com.ensat.entities.Product;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
	   
	@Query(value = "SELECT * FROM product p inner join typeProduct TP ON TP.id = p.product_id", nativeQuery = true)
		Iterable<Product> findAll();
}