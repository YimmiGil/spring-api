package com.ensat.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the product database table.
 * 
 */
@Entity
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String name;

	private int price;

	private String version;

	//bi-directional many-to-one association to Typeproduct
	@ManyToOne
	@JoinColumn(name="product_id")
	private Typeproduct typeproduct;
	
	
	public Product() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return this.price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public Typeproduct getTypeproduct() {
		return this.typeproduct;
	}
	
	public void setTypeproduct(Typeproduct typeproduct) {
		this.typeproduct = typeproduct;
	}

}
