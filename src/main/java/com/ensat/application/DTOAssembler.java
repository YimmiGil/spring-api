package com.ensat.application;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ensat.DTO.ProductDTO;
import com.ensat.entities.Product;

public class DTOAssembler {
	
	public static List<ProductDTO> CreateProducts(Iterable<Product> businessCollections) {
		List<ProductDTO> productsDTO = new ArrayList<ProductDTO>();
        Iterator<Product> it = businessCollections.iterator();

        while (it.hasNext()) {
            Product product = it.next();
            productsDTO.add(CreateProduct(product));
        }
        
        return productsDTO;
	}
	
	public static ProductDTO CreateProduct(Product product) {
		ProductDTO productDTO = new ProductDTO();
		
		productDTO.setDescription(product.getTypeproduct().getDescription());
		productDTO.setNombreProducto(product.getName());
		productDTO.setPrice(product.getPrice());
		productDTO.setId(product.getId());
		
		return productDTO;
	}
	
}
