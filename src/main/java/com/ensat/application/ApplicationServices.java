package com.ensat.application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ensat.DTO.*;
import com.ensat.entities.Product;
import com.ensat.services.ProductService;

@Service
public class ApplicationServices implements IApplicationServices {

	private ProductService productService;
	
    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
	
	@Override
	public List<ProductDTO> getProducts() {			
		return DTOAssembler.CreateProducts(productService.listAllProducts());
	}

}
